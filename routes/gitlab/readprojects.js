const express = require('express')
const router = express.Router()
const Axios = require('axios')

// localhost:3000/projects
router.get('/', async (req, res, next) => {
  try {
    let projectsurl = 'https://gitlab.com/api/v4/users/2136292/projects'
    Axios.get(projectsurl).then(async results => {
      for (const key in await results.data) {
        // console.log(results.data[key].name) // name
        let all = await results.data[key]
        let commitsurl = `https://gitlab.com/api/v4/projects/${all.id}/repository/commits`
        Axios.get(commitsurl).then(async commits => {
          res.send(commits.data) // commit
        })
      }
      // res.send(results.data)
    })
  } catch (error) {
    next(error)
  }
})

module.exports = router
