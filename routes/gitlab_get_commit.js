var express = require('express')
var router = express.Router()
const Axios = require('axios')

/* GET users listing. */
router.get('/', async (req, res, next) => {
  try {
    const urlgitcommit = 'https://gitlab.com/api/v4/projects/13264839/repository/commits'
    const usercommit = await Axios.get(urlgitcommit)
    res.send(usercommit.data)
  } catch (error) {
    next(error)
  }
})

module.exports = router
